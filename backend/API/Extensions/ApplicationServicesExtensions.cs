﻿using System.Linq;
using API.Errors;
using Core.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            {
                // Services
                services.AddScoped<ITokenService, TokenService>();
                services.AddScoped<IOrderService, OrderService>();
                services.AddScoped<IPaymentService, PaymentService>();
                // UnitOfWork
                services.AddScoped<IUnitOfWork, UnitOfWork>();
                // Repositories
                services.AddScoped<IProductRepository, ProductRepository>();
                services.AddScoped<IBasketRepository, BasketRepository>();
                services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

                // Validation Errors Handling Configuration
                services.Configure<ApiBehaviorOptions>(o =>
                {
                    o.InvalidModelStateResponseFactory = actionContext =>
                    {
                        var errors = actionContext.ModelState
                            .Where(e => e.Value.Errors.Count > 0)
                            .SelectMany(x => x.Value.Errors)
                            .Select(x => x.ErrorMessage).ToArray();

                        var errorResponse = new ApiValidationErrorResponse
                        {
                            Errors = errors
                        };

                        return new BadRequestObjectResult(errorResponse);
                    };
                });

                return services;
            }
        }
    }
}