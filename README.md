# Skinet

Project for the Udemy course '[Learn to build and e-commerce app with .Net Core and Angular](https://www.udemy.com/course/learn-to-build-an-e-commerce-app-with-net-core-and-angular)' by Neil Cummings

## Running

### ASP.NET Core

```pwsh
dotnet run -p API
```

### Angular

## Development part

### ASP.NET Core

#### Migrations

- Create Migration

```pwsh
dotnet ef migrations add InitialCreate -p Infrastructure -s API -o Data/Migrations
```

- Apply migrations

```pwsh
dotnet ef database update -p Infrastructure -s API
```

#### DB data init

#### Drop data

```pwsh
dotnet ef database drop -p Infrastructure -s API
```

### Angular
