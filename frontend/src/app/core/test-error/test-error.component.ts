import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-test-error',
  templateUrl: './test-error.component.html',
  styleUrls: ['./test-error.component.scss'],
})
export class TestErrorComponent implements OnInit {
  baseUrl = environment.apiUrl;
  validationErrors: any;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  get404Error(): void {
    this.http.get(`${this.baseUrl}/products/42`).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.error(error);
      }
    );
  }

  get500Error(): void {
    this.http.get(`${this.baseUrl}/buggy/servererror`).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.error(error);
      }
    );
  }

  get400Error(): void {
    this.http.get(`${this.baseUrl}/buggy/badrequest`).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.error(error);
      }
    );
  }

  get400ValidationError(): void {
    this.http.get(`${this.baseUrl}/products/baaaad`).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.error(error);
        this.validationErrors = error.errors;
      }
    );
  }
}
