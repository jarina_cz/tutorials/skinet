import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { ActivatedRoute } from '@angular/router';
import { IOrder } from '../../shared/models/order';
import { BreadcrumbService } from 'xng-breadcrumb';

@Component({
  selector: 'app-order-detailed',
  templateUrl: './order-detailed.component.html',
  styleUrls: ['./order-detailed.component.scss'],
})
export class OrderDetailedComponent implements OnInit {
  order: IOrder;

  constructor(
    private ordersService: OrdersService,
    private activatedRoute: ActivatedRoute,
    private bcService: BreadcrumbService
  ) {
    this.bcService.set('@orderDetails', '');
  }

  ngOnInit(): void {
    this.getOrder();
  }

  getOrder(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.ordersService.getOrder(id).subscribe(
      (order: IOrder) => {
        this.order = order;
        this.bcService.set(
          '@orderDetails',
          `Order# ${order.id} - ${order.status}`
        );
      },
      error => {
        console.log(error);
      }
    );
  }
}
